using UnityEngine;

namespace Game
{
[RequireComponent(typeof(MeshRenderer))]
public class Portal : MonoBehaviour
{
    public Transform PortalOut;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            player.transform.position = new Vector3(PortalOut.transform.position.x, other.transform.position.y, PortalOut.transform.position.z);
        }
    }
}
}