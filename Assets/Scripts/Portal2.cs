using UnityEngine;

namespace Game
{
[RequireComponent(typeof(MeshRenderer))]
public class Portal2 : MonoBehaviour
{
    public Transform PortalOut2;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            player.transform.position = new Vector3(PortalOut2.transform.position.x, other.transform.position.y, PortalOut2.transform.position.z);
        }
    }
}
}